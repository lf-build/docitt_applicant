﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity;
using MongoDB.Bson;

namespace Docitt.Applicant.Mocks
{
    public class FakeApplicantRepository : IApplicantRepository
    {
        public ITenantTime TenantTime { get; set; }
        private List<IApplicant> Applicants { get; } = new List<IApplicant>();

        public FakeApplicantRepository(ITenantTime tenantTime, IEnumerable<IApplicant> applicants) : this(tenantTime)
        {
            Applicants.AddRange(applicants);
        }
        public FakeApplicantRepository(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }


        public Task<IApplicant> Get(string id)
        {
            return Task.Run(() =>
            {
                return Applicants.FirstOrDefault(applicant => applicant.Id.Equals(id));
            });
        }

        public Task<IEnumerable<IApplicant>> All(Expression<Func<IApplicant, bool>> query, int? skip, int? quantity)
        {
            throw new NotImplementedException();
        }

        public void Add(IApplicant applicant)
        {
            applicant.Id = ObjectId.GenerateNewId().ToString();
            Applicants.Add(applicant);
        }

        public void Remove(IApplicant applicant)
        {
            Applicants.Remove(Applicants.FirstOrDefault(a => a.Id.Equals(applicant.Id)));
        }

        public void Update(IApplicant applicant)
        {
            Applicants.Remove(Applicants.FirstOrDefault(a => a.Id.Equals(applicant.Id)));
            Applicants.Add(applicant);
        }

        public IApplicant GetByEmail(string username)
        {
            return Applicants.FirstOrDefault(a => a.Email.Equals(username));
        }

        public IEnumerable<IApplicant> Search(ISearchApplicantRequest searchApplicantRequest)
        {
            var query = Applicants.AsQueryable();

            if (!string.IsNullOrWhiteSpace(searchApplicantRequest.Ssn))
                query = query.Where(applicant => applicant.Ssn.ToLower() == searchApplicantRequest.Ssn.ToLower());
            else if (!string.IsNullOrWhiteSpace(searchApplicantRequest.Email))
                query =
                    query.Where(
                        applicant => applicant.Email.ToLower() == searchApplicantRequest.Email.ToLower());
            else
            {
                if (!string.IsNullOrWhiteSpace(searchApplicantRequest.FirstName))
                    query =
                        query.Where(
                            applicant => applicant.FirstName.ToLower() == searchApplicantRequest.FirstName.ToLower());

                if (!string.IsNullOrWhiteSpace(searchApplicantRequest.LastName))
                    query =
                        query.Where(
                            applicant => applicant.LastName.ToLower() == searchApplicantRequest.LastName.ToLower());

                if (searchApplicantRequest.DateOfBirth.HasValue)
                    query = query.Where(applicant => applicant.DateOfBirth == searchApplicantRequest.DateOfBirth.Value);

            }

            return query.ToList();
        }

        public int Count(Expression<Func<IApplicant, bool>> query)
        {
            throw new NotImplementedException();
        }

        public void UpdateApplicantName(string id, IApplicantNameRequest applicantNameRequest)
        {
            IApplicant existingApplicant = Get(id).Result;
            existingApplicant.FirstName = applicantNameRequest.FirstName;
            existingApplicant.MiddleName = applicantNameRequest.MiddleName;
            existingApplicant.LastName = applicantNameRequest.LastName;
            existingApplicant.GenerationOrSuffix = applicantNameRequest.GenerationOrSuffix;
            Update(existingApplicant);
        }

        public void LinkToUser(string applicantId, string userId)
        {
            /*
            var applicant = Applicants.FirstOrDefault(a => a.Id == applicantId);
            if (applicant == null)
                throw new NotFoundException("Applicant not found");

            applicant.UserIdentity = new UserIdentity
            {
                UserId = userId
            };
            */
        }

        public IApplicant GetByUserId(string userId)
        {

            return Applicants.FirstOrDefault(a => a.UserIdentity != null && a.UserIdentity.UserId == userId);
        }

        public IApplicant GetByToken(string token)
        {
            return Applicants.FirstOrDefault(a => a.UserIdentity != null && a.UserIdentity.Token == token);
        }
    }
}
