﻿namespace LendFoundry.Security.Identity
{
    internal class IUserIdentity
    {
        public string UserId { get; set; }
    }
}