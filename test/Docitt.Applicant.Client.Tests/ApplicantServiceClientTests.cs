﻿using System.Collections.Generic;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using Xunit;
using LendFoundry.Foundation.Client;

namespace Docitt.Applicant.Client.Tests
{
    public class ApplicantServiceClientTests
    {
        private Docitt.Application.Client.ApplicantService ApplicantServiceClient { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> MockServiceClient { get; }

        public ApplicantServiceClientTests()
        {
            MockServiceClient = new Mock<IServiceClient>();
            ApplicantServiceClient = new Application.Client.ApplicantService(MockServiceClient.Object);
        }

        [Fact]
        public void Client_Get_Applicant()
        {
            MockServiceClient.Setup(s => s.Execute<Applicant>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        
                            new Applicant()
                            {
                                Id = "123"
                            }
                            );

            var result = ApplicantServiceClient.Get("123");
            Assert.Equal("/{applicantId}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Client_Add_Applicant()
        {
            var applicant = new ApplicantRequest()
            {
                FirstName = "Nayan"
            };

            MockServiceClient.Setup(s => s.Execute<Applicant>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        applicant);

            var result =  ApplicantServiceClient.Add(applicant);
            Assert.Equal("/", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Client_Update_Applicant()
        {
            var applicant = new Applicant()
            {
                FirstName = "Nayan"
            };

            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(true);

            ApplicantServiceClient.Update("123",applicant);
            Assert.Equal("/{applicantId}", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }

        [Fact]
        public void Client_Search_Applicant()
        {
            MockServiceClient.Setup(s => s.Execute<List<Applicant>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        new List<Applicant>()
                        {
                            new Applicant()
                            {
                                Id = "123"
                            }
                        }
                );

            var result = ApplicantServiceClient.Search(new SearchApplicantRequest());
            Assert.Equal("/search", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Client_Delete_Applicant()
        {
            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r);

            ApplicantServiceClient.Delete("123");
            Assert.Equal("/{applicantId}", Request.Resource);
            Assert.Equal(Method.DELETE, Request.Method);
        }

        [Fact]
        public void Client_UpdateName_ApplicantName()
        {
            var applicantNameRequest = new ApplicantNameRequest()
            {
                FirstName = "Kinjal",
                MiddleName = "N",
                LastName = "Shah",
                GenerationOrSuffix = "Mr."
            };

            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(true);

            ApplicantServiceClient.UpdateName("123", applicantNameRequest, "applicationNumber001");
            Assert.Equal("/{applicantId}/name/{applicationNumber?}", Request.Resource);
            
            Assert.Equal(Method.PUT, Request.Method);
        }
    }
}
