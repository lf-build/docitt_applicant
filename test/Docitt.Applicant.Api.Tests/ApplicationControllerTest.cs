﻿using Docitt.Applicant.Mocks;
using Docitt.Applicant.Api.Controllers;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using System.Collections.Generic;
using Xunit;
using LendFoundry.Foundation.Client;

namespace Docitt.Applicant.Api.Tests
{

    public class ApplicationControllerTest
    {

        [Fact]
        public void AddApplicantReturnApplicantWithIdOnValidApplicant()
        {
            var response = GetController().Add(new ApplicantRequest()
            {
                FirstName = "Nayan",
                LastName = "Paregi",
                Email = "nayan.gp@sigmainfo.net",
                Identities = new List<IIdentity>()
                {
                    new Identity {Name = "LId", Number = "12345"}
                }

            });

            Assert.IsType<HttpOkObjectResult>(response);
            var application = (IApplicant)((HttpOkObjectResult)(response)).Value;

            Assert.NotNull(application);
            Assert.NotNull(application.Id);
        }

        [Fact]
        public void AddApplicantReturnErrorOnIncompleteRequest()
        {
            var response = GetController().Add(new ApplicantRequest());
            Assert.IsType<ErrorResult>(response);
            var result = (ErrorResult)response;
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact(Skip = "Now email duplicate check removed")]
        public void AddApplicantReturnErrorWithDuplicateUsername()
        {
            var dummyApplicants = new List<IApplicant> { new Applicant { Email = "nayan.paregi@gmail.com" } };

            var response = GetController(dummyApplicants).Add(new ApplicantRequest
            {
             
                FirstName = "Nayan",
                LastName = "Paregi",
                Email = "nayan.paregi@gmail.com",
                Identities = new List<IIdentity>()
                {
                    new Identity { Name = "LId", Number = "12345"}
                }
                     
            });
            Assert.IsType<ErrorResult>(response);
            var result = (ErrorResult)response;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);

            var errorDetail = (Error)result.Value;
            Assert.NotNull(errorDetail);
            Assert.Equal("User with same email address is already exist", errorDetail.Message);
        }

        [Fact]
        public void UpdateApplicantReturn404WithInvalidApplicantId()
        {
            var dummyApplicants = new List<IApplicant> { new Applicant { Id="123", Email = "nayan.paregi@gmail.com" } };
            var response = GetController(dummyApplicants).Update("InvalidApplicationNumber", new Applicant
            {
                FirstName = "Nayan",
                LastName = "Paregi",
                Email = "nayan.paregi@gmail.com",
                Identities = new List<IIdentity>()
                {
                    new Identity { Name = "LId", Number = "12345"}
                }
              
            });

            Assert.IsType<ErrorResult>(response);
            var result = (ErrorResult)response;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public void UpdateApplicantReturn204WithValidApplicantId()
        {
            var dummyApplicants = new List<IApplicant> { new Applicant {
                    Email = "nayan@gmail.com",
                    Id = "123",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    MiddleName = "G" } };

            var applicantService = GetApplicantService(dummyApplicants);

            var response = GetController(applicantService).Update("123", new Applicant
            {

                Id = "123",
                FirstName = "Nayan",
                LastName = "Paregi",
                Email = "nayan.paregi@gmail.com",
                Identities = new List<IIdentity>()
                {
                    new Identity { Name = "LId", Number = "12345"}
                }
            });
            Assert.IsType<NoContentResult>(response);

            var applicant = applicantService.Get("123");
            Assert.Equal(applicant.Email, "nayan.paregi@gmail.com");
        }

        [Fact]
        public void GetApplicantReturnErrorOnInvalidApplicantId()
        {
            var dummyApplicants = new List<IApplicant> { new Applicant {
                Email = "nayan.paregi@gmail.com",
                    Id = "123",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    MiddleName = "G" } };
            var response = GetController(dummyApplicants).Get("InvalidNotExist");
            Assert.IsType<ErrorResult>(response);
            var result = (ErrorResult)response;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public void GetApplicantReturnErrorOnNullApplicantId()
        {
            var response = GetController().Get(null);
            Assert.IsType<ErrorResult>(response);
            var result = (ErrorResult)response;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public void GetApplicantReturnApplicantWithValidApplicantId()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    Email = "nayan.paregi@gmail.com",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    MiddleName = "G"
                }
            };

            var response = GetController(dummyApplicants).Get("123");
            Assert.IsType<HttpOkObjectResult>(response);
            var application = (IApplicant)((HttpOkObjectResult)(response)).Value;
            Assert.NotNull(application);
            Assert.NotNull(application.Id);
            Assert.Equal("Nayan",application.FirstName);

        }

        [Fact]
        public void SearcApplicantBySsnReturnCorrectSearchResult()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    Email = "nayan.paregi@gmail.com",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    MiddleName = "G",
                    Ssn = "123"
                },
                new Applicant
                {
                    Id = "456",
                    Email = "nayan.456@gmail.com",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    MiddleName = "G",
                    Ssn = "456"
                }
            };
            var response = GetController(dummyApplicants).Search(new SearchApplicantRequest() { Ssn = "456"});
            Assert.IsType<HttpOkObjectResult>(response);
            var applicants = (List<IApplicant>)((HttpOkObjectResult)(response)).Value;
            Assert.NotNull(applicants);
            Assert.True(applicants.Count==1);
            Assert.Equal("nayan.456@gmail.com", applicants[0].Email);

        }

        [Fact]
        public void DeleteApplicantReturnsNoContent()
        {
            var dummyApplicants = new List<IApplicant>
            {
                new Applicant
                {
                    Id = "123",
                    Email = "nayan.paregi@gmail.com",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    MiddleName = "G",
                    Ssn = "123"
                },
                new Applicant
                {
                    Id = "456",
                    Email = "nayan.456@gmail.com",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    MiddleName = "G",
                    Ssn = "456"
                }
            };
            
            var response = GetController(dummyApplicants).Delete("123");
            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public void DeleteApplicantReturnsNotFound(){
            var response = GetController().Delete("NotExist");
            Assert.IsType<ErrorResult>(response);
            Assert.Equal(404,((ErrorResult)(response)).StatusCode);
        }

        [Fact]
        public void DeleteApplicantReturnsBadRequest()
        {
            var response = GetController().Delete(null);
            Assert.IsType<ErrorResult>(response);
            Assert.Equal(400, ((ErrorResult)(response)).StatusCode);

        }
        [Fact]
        public void UpdateApplicantNameReturn404WithInvalidApplicantId()
        {
            var dummyApplicants = new List<IApplicant> { new Applicant { Id = "123", Email = "nayan.paregi@gmail.com" } };
            var response = GetController(dummyApplicants).UpdateName("InvalidApplicationNumber", new ApplicantNameRequest
            {
                FirstName = "Kinjal",
                MiddleName = "N",
                LastName = "Shah",
                GenerationOrSuffix = "Mr."
            },
            "applicationNumber001");

            Assert.IsType<ErrorResult>(response);
            var result = (ErrorResult)response;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public void UpdateApplicantNameReturn204WithValidApplicantId()
        {
            var dummyApplicants = new List<IApplicant> { new Applicant {
                    Email = "nayan@gmail.com",
                    Id = "123",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    MiddleName = "G" } };
            var dummyApplicantName = new ApplicantNameRequest
            {
                FirstName = "Kinjal",
                MiddleName = "N",
                LastName = "Shah",
                GenerationOrSuffix = "Mr."
            };
            var applicantService = GetApplicantService(dummyApplicants);

            var response = GetController(applicantService).UpdateName("123", dummyApplicantName, "applicationNumber001");
            Assert.IsType<NoContentResult>(response);

            var applicant = applicantService.Get("123");
            Assert.NotNull(applicant);
            Assert.Equal("Kinjal", applicant.FirstName);
            Assert.Equal("N", applicant.MiddleName);
            Assert.Equal("Shah", applicant.LastName);
            Assert.Equal("Mr.", applicant.GenerationOrSuffix);
            
        }

        private static ApplicantController GetController(List<IApplicant> applicants = null)
        {
            return GetController(GetApplicantService(applicants));
        }

        private static ApplicantController GetController(IApplicantService applicantService)
        {
            var applicantController = new ApplicantController(applicantService,null);
            return applicantController;
        }

        private static IApplicantService GetApplicantService(List<IApplicant> applicants = null)
        {
            return new ApplicantService
                (
                    new ApplicantRequestValidator(), 
                    new FakeApplicantRepository(new UtcTenantTime(), applicants ?? new List<IApplicant>()),
                    new Moq.Mock<IEventHubClient>().Object,
                    new UtcTenantTime()
                );
        }
    }
}