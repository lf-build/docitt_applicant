﻿using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace Docitt.Applicant.Client
{
    public static class ApplicantServiceExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddApplicantService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IApplicantServiceFactory>(p => new ApplicantServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IApplicantServiceFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }

        public static IServiceCollection AddApplicantService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IApplicantServiceFactory>(p => new ApplicantServiceFactory(p, uri));
            services.AddTransient(p => p.GetService<IApplicantServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddApplicantService(this IServiceCollection services)
        {
            services.AddTransient<IApplicantServiceFactory>(p => new ApplicantServiceFactory(p));
            services.AddTransient(p => p.GetService<IApplicantServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

       
    }
}
