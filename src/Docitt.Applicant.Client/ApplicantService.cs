﻿
using LendFoundry.Foundation.Client;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace Docitt.Applicant.Client
{
    public class ApplicantService : IApplicantService
    {
        public ApplicantService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public IApplicant Add(IApplicantRequest applicant)
        {
            return Client.PutAsync<IApplicantRequest, Applicant>($"/", applicant, true).Result;
        }

        public void Update(string applicantId, IApplicant applicant)
        {
            Client.PostAsync($"/{applicantId}", applicant, true).Wait();
        }

         public void UpdateApplicantSsnAndDob(string applicantId, ISsnAndDobRequest applicant)
        {
            Client.PostAsync($"/{applicantId}/update-ssn-dob", applicant, true).Wait();
        }

        public IApplicant Get(string applicantId)
        {
            return  Client.GetAsync<Applicant>($"/{applicantId}").Result;
        }

        public void Delete(string applicantId)
        {
            Client.DeleteAsync($"/{applicantId}").Wait();
        }

        public IEnumerable<IApplicant> Search(ISearchApplicantRequest searchApplicantRequest)
        {
            return  Client.PostAsync<ISearchApplicantRequest, List<Applicant>>($"/search", searchApplicantRequest, true).Result;
        }

        public void UpdateName(string applicantId, IApplicantNameRequest applicantNameRequest, string applicationNumber)
        {
            var uri = applicationNumber==null? $"/{applicantId}/name/":$"/{applicantId}/name/{applicationNumber}";
            Client.PutAsync(uri, applicantNameRequest, true).Wait();
        }
        public IApplicant GetByUserId(string userId)
        {
            return  Client.GetAsync<Applicant>($"user/{userId}").Result;
        }

         public async Task<List<IApplicant>> GetApplicantsByUserName(string userName)
        {
            var result = await Client.GetAsync<List<Applicant>>($"/applicants/{userName}");
            return result.ToList<IApplicant>();  
        }

        public void LinkToUser(string applicantId, string userId)
        {
            Client.PutAsync<dynamic>($"/{applicantId}/user/{userId}", null, true).Wait();
        }

        public string CreateIdentityToken(string applicantId)
        {
            return Client.PostAsync<dynamic,dynamic>($"/{applicantId}/create-identity-token", null, true).Result;
        }

        public bool IsTokenValid(string token)
        {
            var response = Client.PostAsync<dynamic,dynamic>($"/identity/token/{{{nameof(token)}}}/verify", null, true).Result;
            return Convert.ToBoolean(response);
        }

        public IApplicant FindByTokenAndLast4Ssn(string token, string lastFourSsn)
        {
            return Client.PostAsync<dynamic,Applicant>($"/find/token/{token}/last4ssn/{lastFourSsn}", null, true).Result;
        }
    }
}