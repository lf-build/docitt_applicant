﻿
using LendFoundry.Security.Tokens;

namespace Docitt.Applicant.Client
{
    public interface IApplicantServiceFactory
    {
        IApplicantService Create(ITokenReader reader);
    }
}
