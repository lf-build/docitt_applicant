﻿using System;

namespace Docitt.Applicant
{
    public class SearchApplicantRequest : ISearchApplicantRequest
    {
        public string Ssn { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
    }
}