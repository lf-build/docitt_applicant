﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Security.Identity;
using System;
using System.Collections.Generic;

namespace Docitt.Applicant
{
    public interface IApplicant : IAggregate
    {
        List<string> Salutation { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string MiddleName { get; set; }
        string Email { get; set; }
        string Ssn { get; set; }
        DateTime? DateOfBirth { get; set; }
        List<IIdentity> Identities { get; set; }
        string GenerationOrSuffix { get; set; }
        IUserIdentity UserIdentity { get; set; }
    }

    public interface ISsnAndDobRequest
    {
        string Ssn { get; set; }
        DateTime? DateOfBirth { get; set; }
        List<IIdentity> Identities { get; set; }
    }

    public class SsnAndDobRequest : ISsnAndDobRequest
    {
        public string Ssn { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public List<IIdentity> Identities { get; set; }
    }
}