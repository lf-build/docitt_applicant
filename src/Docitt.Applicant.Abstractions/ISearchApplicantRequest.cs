﻿using System;

namespace Docitt.Applicant
{
    public interface ISearchApplicantRequest
    {
        string Ssn { get; set; }
        string Email { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        DateTime? DateOfBirth { get; set; }
    }
}