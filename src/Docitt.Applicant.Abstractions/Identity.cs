﻿namespace Docitt.Applicant
{
    public class Identity : IIdentity
    {
        public string Name { get; set; }
        public string Number { get; set; }
    }
}