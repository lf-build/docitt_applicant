﻿namespace Docitt.Applicant.Events
{
    public class UserMappedToApplicant
    {
        public string ApplicantId { get; set; }
        public string UserName { get; set; }
    }
}