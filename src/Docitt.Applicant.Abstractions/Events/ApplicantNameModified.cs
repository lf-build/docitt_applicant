﻿namespace Docitt.Applicant.Events
{
    public class ApplicantNameModified
    {
        public string ApplicationNumber { get; set; }

        public string ApplicantId { get; set; }

        public string OldFirstName { get; set; }

        public string OldLastName { get; set; }

        public string OldMiddleName { get; set; }

        public string OldGenerationOrSuffix { get; set; }

        public string NewFirstName { get; set; }

        public string NewLastName { get; set; }

        public string NewMiddleName { get; set; }

        public string NewGenerationOrSuffix { get; set; }
    }
}