using LendFoundry.Foundation.Persistence;
using LendFoundry.Security.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Applicant
{
    public interface IApplicantRepository : IRepository<IApplicant>
    {
        IApplicant GetByEmail(string username);

        IEnumerable<IApplicant> Search(ISearchApplicantRequest searchApplicantRequest);

        void UpdateApplicantName(string applicantId, IApplicantNameRequest applicantNameRequest);

        IApplicant GetByUserId(string userId);

        Task<List<IApplicant>> GetApplicantsByUsername(string userName);

        IApplicant GetByToken(string token);

        void LinkToUser(string applicantId, string userId);

        void UpdateApplicantSsnAndDob(string id, ISsnAndDobRequest applicant);
    }
}