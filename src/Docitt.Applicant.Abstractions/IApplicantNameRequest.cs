﻿using LendFoundry.Foundation.Persistence;

namespace Docitt.Applicant
{
    public interface IApplicantNameRequest : IAggregate
    {
        string FirstName { get; set; }

        string MiddleName { get; set; }

        string LastName { get; set; }

        string GenerationOrSuffix { get; set; }
    }
}