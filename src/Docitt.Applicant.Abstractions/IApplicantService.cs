﻿using LendFoundry.Security.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Applicant
{
    public interface IApplicantService
    {
        IApplicant Add(IApplicantRequest applicant);
        void Update(string applicantId, IApplicant applicant);
        void UpdateApplicantSsnAndDob(string applicantId, ISsnAndDobRequest applicant);
        IApplicant Get(string applicantId);
        void Delete(string applicantId);
        IEnumerable<IApplicant> Search(ISearchApplicantRequest searchApplicantRequest);
        void UpdateName(string applicantId, IApplicantNameRequest applicantNameRequest, string applicationNumber);
        void LinkToUser(string applicantId, string userId);
        string CreateIdentityToken(string applicantId);
        IApplicant GetByUserId(string userId);
        Task<List<IApplicant>> GetApplicantsByUserName(string userName);
        bool IsTokenValid(string token);
        IApplicant FindByTokenAndLast4Ssn(string token, string lastFourSsn);
    }
}