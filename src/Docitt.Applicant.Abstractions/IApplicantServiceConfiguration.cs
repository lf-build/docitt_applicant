﻿using LendFoundry.Foundation.Client;
namespace Docitt.Applicant
{
    public interface IApplicantServiceConfiguration: IDependencyConfiguration
    {
        int? IdentityTokenExpirationInDays { get; set; }
    }
}