using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using LendFoundry.Security.Identity;

namespace Docitt.Applicant
{
    public class Applicant : Aggregate, IApplicant
    {
        public Applicant()
        {
        }

        public Applicant(string applicantId, IApplicant applicant) : this(applicant)
        {
            Id = applicantId;
        }

        public Applicant(IApplicant applicant)
        {
            if (applicant == null)
                throw new ArgumentNullException(nameof(applicant));

            Salutation = applicant.Salutation;
            FirstName = applicant.FirstName;
            LastName = applicant.LastName;
            MiddleName = applicant.MiddleName;
            Email = applicant.Email;
            Identities = applicant.Identities;
            UserIdentity = applicant.UserIdentity;
            Id = applicant.Id;
            Ssn = applicant.Ssn;
            DateOfBirth = applicant.DateOfBirth;
            GenerationOrSuffix = applicant.GenerationOrSuffix;
        }

        public List<string> Salutation { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string Email { get; set; }

        public string Ssn { get; set; }

        public DateTime? DateOfBirth { get; set; }
        
        [JsonConverter(typeof(InterfaceListConverter<IIdentity, Identity>))]
        public List<IIdentity> Identities { get; set; }

        public string GenerationOrSuffix { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IUserIdentity, UserIdentity>))]
        public IUserIdentity UserIdentity { get; set; }
    }
}