﻿using System.Collections.Generic;

namespace Docitt.Applicant
{
    public class ApplicantServiceConfiguration : IApplicantServiceConfiguration
    {
        public Dictionary<string,string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
        public int? IdentityTokenExpirationInDays { get; set; }
    }
}