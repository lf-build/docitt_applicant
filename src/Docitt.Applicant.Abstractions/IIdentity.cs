﻿namespace Docitt.Applicant
{
    public interface IIdentity
    {
        string Name { get; set; }
        string Number { get; set; }
    }
}