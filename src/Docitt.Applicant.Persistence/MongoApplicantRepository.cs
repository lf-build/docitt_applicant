﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Identity;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Applicant.Persistence
{
    public class MongoApplicantRepository : MongoRepository<IApplicant, Applicant>, IApplicantRepository
    {
        static MongoApplicantRepository()
        {

            BsonClassMap.RegisterClassMap<Applicant>(map =>
            {
                map.AutoMap();
                map.MapProperty(a => a.DateOfBirth).SetSerializer(new NullableSerializer<DateTime>());
                var type = typeof(Applicant);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<Identity>(map =>
            {
                map.AutoMap();
                var type = typeof(Identity);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<UserIdentity>(map =>
            {
                map.AutoMap();
                var type = typeof(UserIdentity);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IUserIdentity, UserIdentity>());
        }

        public MongoApplicantRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "applicants")
        {
            CreateIndexIfNotExists("email", Builders<IApplicant>.IndexKeys.Ascending(i=>i.TenantId).Ascending(i => i.Email));
            CreateIndexIfNotExists("firstname", Builders<IApplicant>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.FirstName));
            CreateIndexIfNotExists("lasttname", Builders<IApplicant>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LastName));
            CreateIndexIfNotExists("ssn", Builders<IApplicant>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Ssn));
            CreateIndexIfNotExists("userid", Builders<IApplicant>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.UserIdentity.UserId));

        }

        public IApplicant GetByEmail(string email)
        {
            if (email == null) throw new ArgumentNullException(nameof(email));
            return Query.FirstOrDefault(applicant => applicant.Email.ToLower() == email.ToLower());
        }

        public IEnumerable<IApplicant> Search(ISearchApplicantRequest searchApplicantRequest)
        {
            var criteria = searchApplicantRequest;
            criteria.Ssn = criteria.Ssn ?? string.Empty;
            criteria.Email = criteria.Email ?? string.Empty;
            criteria.FirstName = criteria.FirstName ?? string.Empty;
            criteria.LastName = criteria.LastName ?? string.Empty;
            var dateTime = criteria.DateOfBirth ?? DateTime.MinValue;
            return
                Query.Where(
                    applicant =>
                        applicant.Ssn.ToLower() == criteria.Ssn.ToLower() ||
                        applicant.Email.ToLower() == criteria.Email.ToLower() ||
                        (
                            applicant.FirstName.ToLower() == criteria.FirstName.ToLower() &&
                            applicant.LastName.ToLower() == criteria.LastName.ToLower() &&
                            applicant.DateOfBirth == dateTime
                        )
                    ).ToList();
        }

        public void UpdateApplicantName(string id, IApplicantNameRequest applicantNameRequest)
        {
            Collection.UpdateOne(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                          a.Id == id),
                Builders<IApplicant>.Update.Set(a => a.FirstName, applicantNameRequest.FirstName)
                    .Set(a => a.MiddleName, applicantNameRequest.MiddleName)
                    .Set(a => a.LastName, applicantNameRequest.LastName)
                    .Set(a => a.GenerationOrSuffix, applicantNameRequest.GenerationOrSuffix));
        }

        public void UpdateApplicantSsnAndDob(string id, ISsnAndDobRequest request)
        {
            Collection.UpdateOne(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                          a.Id == id),
                Builders<IApplicant>.Update.Set(a => a.Ssn, request.Ssn)
                    .Set(a => a.DateOfBirth, request.DateOfBirth)
                    .Set(a => a.Identities, request.Identities));
        }

        public IApplicant GetByUserId(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException(nameof(userId));

            return Query.FirstOrDefault(a => a.UserIdentity != null && a.UserIdentity.UserId == userId);
        }
        public async Task<List<IApplicant>> GetApplicantsByUsername(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentNullException(nameof(userName));

            return await Query.Where(a => a.UserIdentity != null && a.UserIdentity.UserId == userName).ToListAsync();
        }

        //public IApplicant GetByUserIdentityId(string userId)
        //{
        //    var collection = Collection.Database.GetCollection<BsonDocument>("applicants");
        //    var filter = Builders<BsonDocument>.Filter.Eq("UserIdentity._id", userId);
        //    var result = collection.Find(filter).FirstOrDefault();
        //    return result == null ? null : BsonSerializer.Deserialize<IApplicant>(result);
        //}

        public IApplicant GetByToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentNullException(nameof(token));

            return Query.FirstOrDefault(a => a.UserIdentity != null && a.UserIdentity.Token == token);
        }
       

        public void LinkToUser(string applicantId, string userId)
        {
            var identity = new UserIdentity
            {
                UserId = userId,
                Token = null,
                TokenExpiration = null
            };

            var filter = Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id && a.Id == applicantId);
            var update = Builders<IApplicant>.Update.Set(a => a.UserIdentity, identity);

            Collection.UpdateOne(filter, update);
        }
    }
}
