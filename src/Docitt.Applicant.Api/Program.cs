﻿using System;
using Microsoft.AspNetCore.Hosting;

namespace Docitt.Applicant.Api
{
    internal class Program
    {
        public static void Main(string[] args)
        {

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .Start("http://*:5000");

            using (host)
            {
                Console.WriteLine("Started ... Use Ctrl-C to shutdown the host...");
                host.WaitForShutdown();
            }
        }
    }
}