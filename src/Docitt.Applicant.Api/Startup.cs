﻿using FluentValidation;
using Docitt.Applicant.Persistence;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using Jil;
using System.Threading.Tasks;
using System;
using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace Docitt.Applicant.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DocittApplicant"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
               {
                   { "Bearer", new string[] { } }
               });

                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.Applicant.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<ApplicantServiceConfiguration>(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<ApplicantServiceConfiguration>(Settings.ServiceName);
            services.AddTransient<IApplicantServiceConfiguration>(provider => provider.GetRequiredService<IConfigurationService<ApplicantServiceConfiguration>>().Get());

            services.AddTransient<ITenantTime, TenantTime>();
            services.AddTransient<IApplicantService, ApplicantService>();
            services.AddTransient<IApplicantRepository, MongoApplicantRepository>();
            services.AddTransient<IValidator<IApplicant>, ApplicantValidator<IApplicant>>();
            services.AddTransient<IValidator<IApplicantRequest>, ApplicantRequestValidator>();

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();

        }

        public class ErrorHandlingMiddleware
        {
            public ErrorHandlingMiddleware(RequestDelegate next)
            {
                Next = next;
            }

            private RequestDelegate Next { get; }

            public async Task Invoke(HttpContext context)
            {
                try
                {
                    await Next.Invoke(context);
                }
                catch (NotFoundException)
                {
                    var response = context.Response;

                    if (response.HasStarted)
                        return;

                    response.StatusCode = StatusCodes.Status404NotFound;
                }
                catch (InvalidArgumentException invalidArgument)
                {
                    WriteError(StatusCodes.Status400BadRequest, invalidArgument.Message, context.Response);
                }
                catch (Exception)
                {
                    WriteError(StatusCodes.Status500InternalServerError, "We couldn't process your request", context.Response);
                }
            }

            private async void WriteError(int statusCode, string errorMessage, HttpResponse response)
            {
                if (response.HasStarted)
                    return;

                response.StatusCode = statusCode;
                response.ContentType = "application/json";
                await response.WriteAsync(JSON.Serialize(new Error(statusCode, errorMessage)));
            }
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		    app.UseCors(env);
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "DOCITT Applicant Service");
            });

            
         
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();

                  
            app.UseConfigurationCacheDependency();
        }
    }
}