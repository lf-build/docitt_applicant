﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;

namespace Docitt.Applicant.Api.Controllers {

    /// <summary>
    /// Represent applicant controller class.
    /// </summary>
    [Route ("/")]
    public class ApplicantController : ExtendedController {
        /// <summary>
        /// Represent constructor class.
        /// </summary>
        /// <param name="applicantService"></param>
        /// <param name="logger"></param>
        public ApplicantController (IApplicantService applicantService, ILogger logger) : base (logger) {
            ApplicantService = applicantService ?? throw new ArgumentNullException (nameof (applicantService));
        }

        /// <summary>
        /// Applicant service property.
        /// </summary>
        private IApplicantService ApplicantService { get; }

        private static NoContentResult NoContentResult { get; } = new NoContentResult ();

        /// <summary>
        /// Add applicant
        /// </summary>
        /// <param name="applicant">Applicant's input payload</param>
        /// <returns>IApplicant</returns>
        [HttpPut]
        [ProducesResponseType (typeof (IApplicant), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public IActionResult Add ([FromBody] ApplicantRequest applicant) {
            try {
                return Execute (() => Ok (ApplicantService.Add (applicant)));
            } catch (ApplicantWithSameEmailAlreadyExist ex) {
                return ErrorResult.BadRequest (ex.Message);
            } catch (ValidationException validationException) {
                return
                ErrorResult.BadRequest (
                    validationException.Errors.Select (error => new Error (error.ErrorMessage)));
            }
        }
 
        //// NOT Working
        /// <summary>
        /// Update applicant
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="applicant">Applicant's input payload</param>
        /// <returns>NoContentResult</returns>
        [HttpPost ("/{applicantId}")]
        [ProducesResponseType (typeof (NoContentResult), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public IActionResult Update (string applicantId, [FromBody] Applicant applicant) {
            try {
                return Execute (() => {
                    ApplicantService.Update (applicantId, applicant);
                    return NoContentResult;
                });
            } catch (ApplicantWithSameEmailAlreadyExist ex) {
                return ErrorResult.BadRequest (ex.Message);
            } catch (ValidationException validationException) {
                return
                ErrorResult.BadRequest (
                    validationException.Errors.Select (error => new Error (error.ErrorMessage)));
            }
        }

        /// <summary>
        /// Update applicant GivenData
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="applicant">Applicant's input payload</param>
        /// <returns>NoContentResult</returns>
        [HttpPost ("/{applicantId}/update-ssn-dob")]
        [ProducesResponseType (typeof (NoContentResult), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public IActionResult UpdateApplicantSsnAndDob(string applicantId, [FromBody] SsnAndDobRequest applicant) {
            try {
                return Execute (() => {
                    ApplicantService.UpdateApplicantSsnAndDob(applicantId, applicant);
                    return NoContentResult;
                });
            } catch (ApplicantWithSameEmailAlreadyExist ex) {
                return ErrorResult.BadRequest (ex.Message);
            } catch (ValidationException validationException) {
                return
                ErrorResult.BadRequest (
                    validationException.Errors.Select (error => new Error (error.ErrorMessage)));
            }
        }


        /// <summary>
        /// Get applicant
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <returns>IApplicant</returns>
        [HttpGet ("/{applicantId}")]
        [ProducesResponseType (typeof (IApplicant), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public IActionResult Get (string applicantId) {
            return Execute (() => Ok (ApplicantService.Get (applicantId)));
        }

        /// <summary>
        /// Delete applicant
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <returns>NoContentResult</returns>
        [HttpDelete ("/{applicantId}")]
        [ProducesResponseType (typeof (NoContentResult), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public IActionResult Delete (string applicantId) {
            return Execute (() => {
                ApplicantService.Delete (applicantId);
                return NoContentResult;
            });
        }

        /// <summary>
        /// Search applicant
        /// </summary>
        /// <param name="searchApplicantRequest">searchApplicantRequest</param>
        /// <returns>IApplicant[]</returns>
        [HttpPost ("/search")]
        [ProducesResponseType (typeof (IApplicant[]), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public IActionResult Search ([FromBody] SearchApplicantRequest searchApplicantRequest) {
            return Execute (() => Ok (ApplicantService.Search (searchApplicantRequest)));
        }

        /// <summary>
        /// Update Name of applicant
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="applicantNameRequest">applicantNameRequest</param>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <returns>NoContentResult</returns>
        [HttpPut ("/{applicantId}/name/{applicationNumber?}")]

        [ProducesResponseType (typeof (NoContentResult), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]

        public IActionResult UpdateName (string applicantId, [FromBody] ApplicantNameRequest applicantNameRequest, string applicationNumber) {
            return Execute (() => {
                ApplicantService.UpdateName (applicantId, applicantNameRequest, applicationNumber);
                return NoContentResult;
            });
        }

        /// <summary>
        /// LinkToUser
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="userId">userId</param>
        /// <returns>NoContentResult</returns>
        [HttpPut ("/{applicantId}/user/{userId}")]

        [ProducesResponseType (typeof (NoContentResult), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]

        public IActionResult LinkToUser (string applicantId, string userId) {
            return Execute (() => {
                ApplicantService.LinkToUser (applicantId, userId);
                return NoContentResult;
            });
        }

        /// <summary>
        /// GetByUserId
        /// </summary>
        /// <param name="userId">userId</param>
        /// <returns>IApplicant</returns>
        [HttpGet ("user/{userId}")]

        [ProducesResponseType (typeof (IApplicant), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]

        public IActionResult GetByUserId (string userId) {
            return Execute (() => Ok (ApplicantService.GetByUserId (userId)));
        }

        /// <summary>
        /// CreateIdentityToken
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>string</returns>
        [HttpPost ("/{id}/create-identity-token")]

        [ProducesResponseType (typeof (string), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]

        public IActionResult CreateIdentityToken (string id) {
            return Execute (() => Ok (ApplicantService.CreateIdentityToken (id)));
        }

        /// <summary>
        /// GetByUserIdentityId
        /// </summary>
        /// <param name="identityId">identityId</param>
        /// <returns>IApplicant</returns>
        [HttpGet ("/identity/id/{identityId}")]

        [ProducesResponseType (typeof (IApplicant), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]

        public IActionResult GetByUserIdentityId (string identityId) {
            return Execute (() => Ok (ApplicantService.GetByUserId (identityId)));
        }

        /// <summary>
        /// GetApplicantsbyUserName
        /// </summary>
        /// <param name="userName">identiuserNametyId</param>
        /// <returns>applicants></returns>
        [HttpGet ("/applicants/{userName}")]
        [ProducesResponseType (typeof (List<Applicant>), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public Task<IActionResult> GetApplicantsbyUserName (string userName) {
            return ExecuteAsync (async () => Ok (await ApplicantService.GetApplicantsByUserName (userName)));
        }
        

        /// <summary>
        /// VerifyToken
        /// </summary>
        /// <param name="token">token</param>
        /// <returns>bool</returns>
        [HttpPost ("/identity/token/{token}/verify")]

        [ProducesResponseType (typeof (bool), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]

        public IActionResult VerifyToken (string token) {
            return Execute (() => {
                if (ApplicantService.IsTokenValid (token))
                    return NoContentResult;

                return new NotFoundResult ();

            });
        }

        /// <summary>
        /// FindByTokenAndLastFourSsn
        /// </summary>
        /// <param name="token">token</param>
        /// <param name="last4Ssn">last4Ssn</param>
        /// <returns>IApplicant</returns>
        [HttpGet ("/find/token/{token}/last4ssn/{last4Ssn}")]

        [ProducesResponseType (typeof (IApplicant), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]

        public IActionResult FindByTokenAndLastFourSsn (string token, string last4Ssn) {
            return Execute (() => Ok (ApplicantService.FindByTokenAndLast4Ssn (token, last4Ssn)));
        }
    }
}