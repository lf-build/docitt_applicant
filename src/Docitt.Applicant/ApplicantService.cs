﻿using FluentValidation;
using Docitt.Applicant.Events;

using System;
using System.Collections.Generic;
using LendFoundry.Security.Identity;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.EventHub;
using System.Threading.Tasks;

namespace Docitt.Applicant
{
    public class ApplicantService : IApplicantService
    {
        public ApplicantService
        (
            IValidator<IApplicantRequest> applicantRequestValidator,
            IApplicantRepository applicantRepository,
            IEventHubClient eventHub,
            ITenantTime tenantTime,
            IApplicantServiceConfiguration configuration
        )
        {
            ApplicantRequestValidator = applicantRequestValidator ?? throw new ArgumentNullException(nameof(applicantRequestValidator));
            ApplicantRepository = applicantRepository ?? throw new ArgumentNullException(nameof(applicantRepository));
            EventHub = eventHub ?? throw new ArgumentNullException(nameof(eventHub));
            TenantTime = tenantTime ?? throw new ArgumentNullException(nameof(tenantTime));
            Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        private IValidator<IApplicantRequest> ApplicantRequestValidator { get; }

        private IApplicantRepository ApplicantRepository { get; }

        private IEventHubClient EventHub { get; }

        private ITenantTime TenantTime { get; }
        private IApplicantServiceConfiguration Configuration { get; }

        public IApplicant Add(IApplicantRequest applicant)
        {
            var applicantModel = new Applicant(applicant);

            var result = ApplicantRequestValidator.Validate(applicant);
            if (!result.IsValid)
                throw new ValidationException(result.Errors);

            ApplicantRepository.Add(applicantModel);
            return applicantModel;
        }

        public void Update(string applicantId, IApplicant applicant)
        {
            //TODO: Do we need to allow change Applicant's email address?
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));
            var applicantModel = new Applicant(applicantId, applicant);

            var existingApplicant = Get(applicantId);
            if (!string.Equals(existingApplicant.Email, applicantModel.Email) && ApplicantRepository.GetByEmail(applicantModel.Email) != null)
                throw new ApplicantWithSameEmailAlreadyExist("User with same email address is already exist");

            ApplicantRepository.Update(applicantModel); //// Not updating the values
        }

         public void UpdateApplicantSsnAndDob(string applicantId, ISsnAndDobRequest applicant)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));

            ApplicantRepository.UpdateApplicantSsnAndDob(applicantId, applicant);
        }


        public IApplicant Get(string applicantId)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));

            var applicant = ApplicantRepository.Get(applicantId).Result;
            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            return applicant;
        }

        public void Delete(string applicantId)
        {
            ApplicantRepository.Remove(Get(applicantId));
        }

        public IEnumerable<IApplicant> Search(ISearchApplicantRequest searchApplicantRequest)
        {
            if (searchApplicantRequest == null)
                throw new ArgumentNullException(nameof(searchApplicantRequest));

            return ApplicantRepository.Search(searchApplicantRequest);
        }

        public void UpdateName(string applicantId, IApplicantNameRequest applicantNameRequest, string applicationNumber)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));

            if (applicantNameRequest == null)
                throw new ArgumentNullException(nameof(applicantNameRequest));

            if (string.IsNullOrWhiteSpace(applicantNameRequest.FirstName))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantNameRequest.FirstName));

            if (string.IsNullOrWhiteSpace(applicantNameRequest.LastName))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantNameRequest.LastName));

            var applicant = ApplicantRepository.Get(applicantId).Result;
            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            var eventNameModified = new ApplicantNameModified
            {
                ApplicationNumber = applicationNumber,
                ApplicantId = applicant.Id,
                OldFirstName = applicant.FirstName,
                OldLastName = applicant.LastName,
                OldMiddleName = applicant.MiddleName,
                OldGenerationOrSuffix = applicant.GenerationOrSuffix,
                NewFirstName = applicantNameRequest.FirstName,
                NewLastName = applicantNameRequest.LastName,
                NewMiddleName = applicantNameRequest.MiddleName,
                NewGenerationOrSuffix = applicantNameRequest.GenerationOrSuffix
            };

            ApplicantRepository.UpdateApplicantName(applicantId, applicantNameRequest);

            EventHub.Publish(nameof(ApplicantNameModified), eventNameModified).Wait();
        }
        
        public void LinkToUser(string applicantId, string userId)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));

            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentException("Argument is null or whitespace", nameof(userId));

            ApplicantRepository.LinkToUser(applicantId, userId);

            EventHub.Publish(nameof(UserMappedToApplicant), new UserMappedToApplicant()
            {
                ApplicantId = applicantId,
                UserName = userId
            }).Wait();
        }

        public string CreateIdentityToken(string applicantId)
        {
            var identity = new UserIdentity
            {
                Token = $"{Guid.NewGuid()}",
                TokenExpiration =
                    new TimeBucket(DateTime.UtcNow.AddDays(Configuration.IdentityTokenExpirationInDays ?? 5))
            };

            var applicant = Get(applicantId);

            if (applicant == null || applicant.UserIdentity != null)
                throw new NotFoundException("Applicant not found.");

            applicant.UserIdentity = identity;
            ApplicantRepository.Update(applicant);

            return identity.Token;

        }


        public IApplicant GetByUserId(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new InvalidArgumentException("User ID cannot be empty.");

            var applicant= ApplicantRepository.GetByUserId(userId);

            if (applicant == null)
                throw new NotFoundException("Applicant not found.");

            return applicant;
        }

        public async Task<List<IApplicant>> GetApplicantsByUserName(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new InvalidArgumentException("userName cannot be empty.");

            var applicants = await ApplicantRepository.GetApplicantsByUsername(userName);

            if (applicants == null)
                throw new NotFoundException("Applicant not found.");

            return applicants;
        }

        public bool IsTokenValid(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentException("Token cannot be empty.", nameof(token));

            var applicant = ApplicantRepository.GetByToken(token);
            if (applicant?.UserIdentity != null)
            {
                var now = TenantTime.Now;
                var expiration = TenantTime.Convert(applicant.UserIdentity.TokenExpiration.Time);
                Console.WriteLine($"{now} - {expiration}");
                return expiration > now;
            }
            return false;
        }

        public IApplicant FindByTokenAndLast4Ssn(string token, string last4Ssn)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentException("Argument is null or whitespace", nameof(token));

            if (string.IsNullOrWhiteSpace(last4Ssn))
                throw new ArgumentException("Argument is null or whitespace", nameof(last4Ssn));

            var applicant = ApplicantRepository.GetByToken(token);

            if (applicant == null || !IsTokenValid(token))
                throw new ArgumentException("Invalid token. Applicant not found or token expired.", nameof(token));

            if(!applicant.Ssn.Substring(applicant.Ssn.Length - 4, 4).Equals(last4Ssn))
                throw new ArgumentException("Invalid last four ssn digits", nameof(last4Ssn));

            return applicant;
        }
    }
}